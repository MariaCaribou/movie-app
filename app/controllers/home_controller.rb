class HomeController < ApplicationController
  def index
    @movies = Movie.all.to_a
    @first_movie = @movies.shift
  end
end