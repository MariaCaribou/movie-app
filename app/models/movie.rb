class Movie < ApplicationRecord
    validates_uniqueness_of :name
    has_one_attached :image
end
