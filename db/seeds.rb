# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

movie1 = Movie.create(name: 'Lost in Translation', position: 1, image_link: '')
movie1.image.attach(io: File.open(File.join(Rails.root, 'storage/lost-in-translation-cover.jpg')), filename: 'lost-in-translation-cover.jpg')

movie2 = Movie.create(name: 'Old Boy', position: 2, image_link: '')
movie2.image.attach(io: File.open(File.join(Rails.root, 'storage/old-boy-cover.jpg')), filename: 'old-boy-cover.jpg')

movie3 = Movie.create(name: 'Moonrise Kingdom', position: 3, image_link: '')
movie3.image.attach(io: File.open(File.join(Rails.root, 'storage/moonrise-kingdom-cover.jpg')), filename: 'moonrise-kingdom-cover.jpg')

movie4 = Movie.create(name: 'Avengers Endgame', position: 4, image_link: '')
movie4.image.attach(io: File.open(File.join(Rails.root, 'storage/avengers-endgame-cover.jpg')), filename: 'avengers-endgame-cover.jpg')

movie5 = Movie.create(name: 'Melancholia', position: 5, image_link: '')
movie5.image.attach(io: File.open(File.join(Rails.root, 'storage/melancholia-cover.jpg')), filename: 'melancholia-cover.jpg')